cc=cc

target-dir=./target/
src-dir=./src/
obj-dir=$(target-dir)obj/

srcs=$(wildcard $(src-dir)*)
objs=$(addsuffix .o,$(addprefix $(obj-dir),$(notdir $(srcs))))

target=$(target-dir)target-name

CFLAGS=-std=c99
LD_FLAGS=

all: $(target)

run: $(target)
	@$(target)

$(target): $(objs)
	$(cc) -o $@ $^ $(CFLAGS) $(LD_FLAGS)

$(obj-dir)%.c.o: $(src-dir)%.c
	$(cc) -c -o $@ $^ $(CFLAGS)

clean:
	rm -rf $(target) $(obj-dir)*
